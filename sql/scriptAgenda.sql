CREATE DATABASE `grupo_12`;
USE grupo_12;
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Calle` varchar(45) NOT NULL,
  `Altura` varchar(5) NOT NULL,
  `Piso` varchar(5),
  `Departamento` varchar(5),
  `Localidad` varchar(20) NOT NULL,
  `Email` varchar(45) ,
  `FechaNacimiento` varchar(20) NOT NULL,
  `TipoContacto` varchar(20) NOT NULL,
  PRIMARY KEY (`idPersona`)
);

CREATE TABLE `localidad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `tipocontacto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `hobby` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
