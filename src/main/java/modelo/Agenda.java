package modelo;

import java.util.List;

import dto.HobbyDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.HobbyDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private TipoContactoDAO tipoContacto;
	private HobbyDAO hobbie;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipoContacto = metodo_persistencia.createContactoDAO();
		this.hobbie = metodo_persistencia.createHobbieDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public void editarPersona(PersonaDTO persona_a_editar) {
		this.persona.update(persona_a_editar);
	}

	public void agregarLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}
	
	public void agregarHobbie(HobbyDTO nuevoHobbie)
	{
		this.hobbie.insert(nuevoHobbie);
	}
	
	public boolean validarLocalidad(LocalidadDTO nuevaLocalidad)
	{
		return this.localidad.validate(nuevaLocalidad);
	}

	
	public boolean validarTipoContacto(TipoContactoDTO tipoContacto)
	{
		return this.tipoContacto.validate(tipoContacto);
	}
	

	public boolean validarHobbie(HobbyDTO hobbie)
	{
		return this.hobbie.validate(hobbie);
	}
	
	public void agregarTipoContacto(TipoContactoDTO nuevoContacto)
	{
		this.tipoContacto.insert(nuevoContacto);
	}
	
	
	
	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void borrarLocalidad(LocalidadDTO localidad_a_eliminar)
	{
		this.localidad.delete(localidad_a_eliminar);
	}
	
	public void borrarTipoContacto(TipoContactoDTO tipo_contacto_a_eliminar)
	{
		this.tipoContacto.delete(tipo_contacto_a_eliminar);
	}
	
	public void borrarHobbie(HobbyDTO hobby_a_eliminar)
	{
		this.hobbie.delete(hobby_a_eliminar);
	}
	
	public void modificarLocalidad(LocalidadDTO localidad_a_modificar)
	{
		this.localidad.update(localidad_a_modificar);
	}
	
	public void modificarTipoContacto(TipoContactoDTO tipo_contacto_a_modificar)
	{
		this.tipoContacto.update(tipo_contacto_a_modificar);
	}
	
	public void modificarHobbie(HobbyDTO hobby_a_modificar)
	{
		this.hobbie.update(hobby_a_modificar);
	}
	
	
	public PersonaDTO obtenerPersona(PersonaDTO persona_a_leer) {
		return this.persona.read(persona_a_leer);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public List<PersonaDTO> pruebOobtener()
	{
		return this.persona.getPersonas(new HobbyDTO(1,"Boxeo"));
	}
	
	public List<LocalidadDTO> obtenerLocalidad()
	{
		return this.localidad.readAll();
	}
	
	public List<TipoContactoDTO> obtenerTipoContacto()
	{
		return this.tipoContacto.readAll();
	}
	
	public List<HobbyDTO> obtenerHobbies()
	{
		return this.hobbie.readAll();
	}
	
	public List<HobbyDTO> obtenerPersonasHobbies()
	{
		return this.hobbie.readAllHobbyPersons();
	}
}
