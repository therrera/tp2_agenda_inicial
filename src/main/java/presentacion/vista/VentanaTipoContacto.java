package presentacion.vista;

import java.awt.Component;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;

public class VentanaTipoContacto extends JFrame {

	private static final long serialVersionUID = 1L;	
	private static VentanaTipoContacto INSTANCE;
	private JTextField txtDescripcion;
	private JButton btnAgregar; 
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnModificar;
	private JButton btnEliminar;
	private DefaultTableModel modelTipoContacto;
	private  String[] nombreColumnas = {"Id","Descripcion",""};
	private JTextField txtId_ME;
	private JTextField txtDescripcion_ME;
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public DefaultTableModel getModelTipoContacto() {
		return modelTipoContacto;
	}

	public void setModelTipoContacto(DefaultTableModel modelTipoContacto) {
		this.modelTipoContacto = modelTipoContacto;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public JTextField getTxtId_ME() {
		return txtId_ME;
	}

	public void setTxtId_ME(JTextField txtId_ME) {
		this.txtId_ME = txtId_ME;
	}

	public JTextField getTxtDescripcion_ME() {
		return txtDescripcion_ME;
	}

	public void setTxtDescripcion_ME(JTextField txtDescripcion_ME) {
		this.txtDescripcion_ME = txtDescripcion_ME;
	}

	public void mostrar()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtDescripcion() {
		return txtDescripcion;
	}

	public void setTxtDescripcion(JTextField txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public VentanaTipoContacto()
	{
		super();
		setTitle("Tipo de contacto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 639, 650);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(54, 27, 503, 114);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(25, 25, 136, 20);
		panel.add(lblDescripcion);
	
		txtDescripcion = new JTextField();
		txtDescripcion.setBounds(146, 22, 309, 26);
		panel.add(txtDescripcion);
		txtDescripcion.setColumns(10);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(340, 64, 115, 29);
		panel.add(btnAgregar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(54, 158, 503, 218);
		getContentPane().add(scrollPane);	
		
		table = new JTable();
		
		modelTipoContacto = new DefaultTableModel(null,nombreColumnas);
		table.setModel(modelTipoContacto);
		
		table.getColumnModel().getColumn(0).setPreferredWidth(103);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setResizable(false);
	//	table.getColumnModel().getColumn(2).setPreferredWidth(100);
	//	table.getColumnModel().getColumn(2).setResizable(false);
		
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setBounds(54, 400, 503, 163);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setBounds(96, 28, 69, 20);
		panel_1.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Descripcion");
		lblNewLabel_2.setBounds(39, 64, 148, 20);
		panel_1.add(lblNewLabel_2);
		
		txtId_ME = new JTextField();
		txtId_ME.setEditable(false);
		txtId_ME.setBounds(151, 25, 299, 26);
		panel_1.add(txtId_ME);
		txtId_ME.setColumns(10);
		
		txtDescripcion_ME = new JTextField();
		txtDescripcion_ME.setBounds(151, 64, 299, 26);
		panel_1.add(txtDescripcion_ME);
		txtDescripcion_ME.setColumns(10);
		
	    btnModificar = new JButton("Modificar");
		btnModificar.setBounds(205, 106, 115, 29);
		panel_1.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(335, 106, 115, 29);
		panel_1.add(btnEliminar);
	}
	
	public boolean eliminarMensaje()
	{
		int confirmado = JOptionPane.showConfirmDialog(null,"¿Confirma que desea eliminar el tipo de contacto?");
		if (JOptionPane.OK_OPTION == confirmado)
		   return true;
		else
		   return false;
	}
	

	public static VentanaTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaTipoContacto(); 	
			return new VentanaTipoContacto();
		}
		else
			return INSTANCE;
	}
	
	public void mostrarMensaje(String mensaje, String titulo)
	{
		JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.DEFAULT_OPTION);
	}
	
	
	public void llenarTabla(List<TipoContactoDTO> contactos) {
		this.getModelTipoContacto().setRowCount(0); //Para vaciar la tabla
		this.getModelTipoContacto().setColumnCount(0);
		this.getModelTipoContacto().setColumnIdentifiers(this.getNombreColumnas());
		//this.getTable().getColumnModel().getColumn(2).setCellRenderer(new Button("AA"));
		this.getTable().getColumnModel().getColumn(2).setCellRenderer(new botonseleccionarTipoContacto());
		
		
		for (TipoContactoDTO l : contactos)
		{
			int id = l.getId();
			String descripcion = l.getDescripcion();
			Object[] fila = {id, descripcion,new JButton("Modificar")};
			this.getModelTipoContacto().addRow(fila);
			table.setModel(modelTipoContacto);
		}
		
	}
	
	class botonseleccionarTipoContacto extends JButton implements TableCellRenderer {
		
		private static final long serialVersionUID = 1L;

		public botonseleccionarTipoContacto()
		 {
			   this.setText("Seleccionar");
		 }

		  public Component getTableCellRendererComponent(JTable table, Object value,
		      boolean isSelected, boolean hasFocus, int row, int column) {
			  if (isSelected) {
				  	String id=table.getModel().getValueAt(row, 0).toString();
				  	String desc=table.getModel().getValueAt(row, 1).toString();
				  	txtId_ME.setText(id);
				  	txtDescripcion_ME.setText(desc);
		    		}
		    return this;
		  }
		}
}
