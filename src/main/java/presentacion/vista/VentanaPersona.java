package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.HobbyDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.LocalidadDAO;
import presentacion.controlador.Validador;

import javax.swing.JComboBox;
import java.awt.Label;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.awt.Font;
import com.toedter.calendar.JDateChooser;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepartamento;
	private JTextField txtEmail;
	private JButton btnAgregarPersona;
	private static VentanaPersona INSTANCE;
	private JComboBox<String> jComboBoxLocalidad;
	private JComboBox<String> jComboBoxTipoContacto;
	private JComboBox<String> cbHobbie; 
	
	
	public JComboBox<String> getCbHobbie() {
		return cbHobbie;
	}

	public void setCbHobbie(JComboBox<String> cbHobbie) {
		this.cbHobbie = cbHobbie;
	}


	private static boolean ingresoPisoDepartamento;
	private JDateChooser dateChooserFechaNacimiento = new JDateChooser();
	
	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() 
	{
		super();
		setTitle("Personas");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 536, 521);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(39, 16, 534, 462);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 136, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 41, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblCalle= new JLabel("Calle");
		lblCalle.setBounds(10, 71, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura= new JLabel("Altura");
		lblAltura.setBounds(10, 101, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso= new JLabel("Piso");
		lblPiso.setBounds(10, 131, 113, 14);
		panel.add(lblPiso);
		
		JLabel lblDepartamento= new JLabel("Departamento");
		lblDepartamento.setBounds(10, 161, 113, 14);
		panel.add(lblDepartamento);
		
		JLabel lblLocalidad= new JLabel("Localidad");
		lblLocalidad.setBounds(10, 200, 113, 14);
		panel.add(lblLocalidad);
		
		JLabel lblTipoContacto= new JLabel("Tipo de Contacto");
		lblTipoContacto.setBounds(10, 307, 133, 14);
		panel.add(lblTipoContacto);
		
		Label lblEmail = new Label("E-Mail");
		lblEmail.setFont(new Font("Dialog", Font.BOLD, 12));
		lblEmail.setBounds(10, 237, 68, 21);
		panel.add(lblEmail);
		
		JLabel lblFechaNacimiento= new JLabel("Fecha de Nacimiento");
		lblFechaNacimiento.setBounds(10, 272, 156, 14);
		panel.add(lblFechaNacimiento);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(184, 9, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(184, 39, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(184, 69, 164, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(184, 99, 164, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(184, 129, 164, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDepartamento = new JTextField();
		txtDepartamento.setBounds(184, 159, 164, 20);
		panel.add(txtDepartamento);
		txtDepartamento.setColumns(10);
		
		jComboBoxLocalidad = new JComboBox<String>();
		jComboBoxLocalidad.setBounds(184, 195, 164, 24);
		panel.add(jComboBoxLocalidad);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(184, 238, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		jComboBoxTipoContacto = new JComboBox<String>(); 
		jComboBoxTipoContacto.setBounds(184, 302, 164, 24);
		panel.add(jComboBoxTipoContacto);

		dateChooserFechaNacimiento = new JDateChooser();
		dateChooserFechaNacimiento.setBounds(184, 272, 164, 19);
		panel.add(dateChooserFechaNacimiento);
		
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(235, 388, 113, 23);
		panel.add(btnAgregarPersona);
		
		JLabel label = new JLabel("(*)");
		label.setBounds(352, 8, 35, 20);
		panel.add(label);
		
		JLabel label_1 = new JLabel("(*)");
		label_1.setBounds(352, 38, 35, 20);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("(*)");
		label_2.setBounds(352, 68, 35, 20);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("(*)");
		label_3.setBounds(352, 98, 35, 20);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("(*)");
		label_4.setBounds(352, 197, 35, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("(*)");
		label_5.setBounds(352, 237, 35, 20);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("(*)");
		label_6.setBounds(352, 269, 35, 20);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setBounds(352, 304, 35, 20);
		panel.add(label_7);
		
		JLabel lblCambiosObligatorios = new JLabel("(*) Campos obligatorios");
		lblCambiosObligatorios.setBounds(15, 408, 184, 20);
		panel.add(lblCambiosObligatorios);
		
		JLabel lblHobbie = new JLabel("Hobbie");
		lblHobbie.setBounds(13, 348, 133, 14);
		panel.add(lblHobbie);
		
		cbHobbie = new JComboBox<String>();
		cbHobbie.setBounds(184, 342, 164, 24);
		panel.add(cbHobbie);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setBounds(352, 345, 35, 20);
		panel.add(label_8);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}
	
	public JTextField getTxtPiso() {
		return txtPiso;
	}
	
	public JTextField getTxtDepartamento() {
		return txtDepartamento;
	}
	
	public JComboBox<String> getjComboBoxLocalidad() {
		return jComboBoxLocalidad;
	}
	
	public JTextField getTxtEmail() {
		return txtEmail;
	}
	
	public String getFechaNacimiento() {
		String pattern = "dd/MM/yyyy";
		DateFormat formatter = new SimpleDateFormat(pattern);
		if(dateChooserFechaNacimiento != null)
			return  formatter.format(dateChooserFechaNacimiento.getDate()).toString();
		else
			return null;
	}
	
	public JComboBox<String> getjComboBoxTipoContacto() {
		return jComboBoxTipoContacto;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtPiso.setText(null);
		this.txtDepartamento.setText(null);
		this.txtEmail.setText(null);
		this.dateChooserFechaNacimiento.setDate(null);
	
		this.dispose();
	}
	
	public boolean verificarCampos() {
		Validador validador = new Validador();
		
		boolean boolNombre;
		boolNombre = validador.validarTexto(txtNombre.getText());
		
		boolean boolTelefono;
		boolTelefono = validador.validarNumerico(txtTelefono.getText(), false);
		
		boolean boolAltura;
		boolAltura = validador.validarNumerico(txtAltura.getText(), false);
		
		boolean boolPiso;
		boolean boolDepartamento;
		if(txtPiso.getText().equals("") && txtDepartamento.getText().equals("")) {
			boolPiso = true;
			boolDepartamento = true;
		}
		else if ( (txtPiso.getText().equals("") && ! txtDepartamento.getText().equals("") ) || ( ! txtPiso.getText().equals("") && txtDepartamento.getText().equals("") ) ) {
			boolPiso = false;
			boolDepartamento = false;
		}
		else {
			boolPiso = validador.validarNumerico(txtPiso.getText(), true);
			boolDepartamento = validador.validarAlfaNumerico(txtDepartamento.getText());
		}
		
		boolean boolEmail;
		boolEmail = validador.validarEmail(txtEmail.getText());
		
		boolean boolFecha;
		boolFecha = validador.validarFecha(getFechaNacimiento());
		
		return boolTelefono && boolAltura && boolNombre  && boolPiso && boolDepartamento && boolEmail && boolFecha;
	}
	
	public void llenarComboLocalidades(List<LocalidadDTO> localidades) {
	
		jComboBoxLocalidad.removeAllItems();
		for (LocalidadDTO p : localidades)
		{
			jComboBoxLocalidad.addItem(p.getDescripcion().toString());
		}
	}
	
	public void llenarComboHobbies(List<HobbyDTO> hobby) {
		cbHobbie.removeAllItems();
		for (HobbyDTO p : hobby)
		{
			cbHobbie.addItem(p.getDescripcion().toString());
		}
	}
	
	
	public void llenarComboTipoContanto(List<TipoContactoDTO> contactos) {
		
		jComboBoxTipoContacto.removeAllItems();
		for (TipoContactoDTO p : contactos)
		{
			jComboBoxTipoContacto.addItem(p.getDescripcion());
		}
	}
}

