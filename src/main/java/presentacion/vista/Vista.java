package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnEditar;
	private JButton btnReporte2;

	private JButton btnReporte;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono", "Calle", "Altura", "Piso", "Departamento", "Localidad", "E-Mail", "Fecha de Nacimiento", "Tipo de Contacto","Hobbie"};

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 1455, 401);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1381, 307);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 1364, 252);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(2).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(2).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(3).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(3).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(4).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(4).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(5).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(5).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(6).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(6).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(7).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(7).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(8).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(8).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(9).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(9).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(10).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(10).setResizable(false);
		
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(12, 275, 97, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(121, 275, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(222, 275, 89, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(323, 275, 106, 23);
		panel.add(btnReporte);
		
		btnReporte2 = new JButton("Reporte 2");
		btnReporte2.setBounds(444, 272, 115, 29);
		panel.add(btnReporte2);
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Menu");
		menuBar.add(mnNewMenu);
		
		 itLocalidades_vista = new JMenuItem("Localidades");
		mnNewMenu.add(itLocalidades_vista);
		
		 itTipoContacto_vista = new JMenuItem("Tipos de contactos");
		mnNewMenu.add(itTipoContacto_vista);
		
		itHobby_vista = new JMenuItem("Hobbies");
		mnNewMenu.add(itHobby_vista);
	}
	
	private JMenuItem itLocalidades_vista;
	private JMenuItem itTipoContacto_vista;
	private JMenuItem itHobby_vista;
	
	public JMenuItem getItHobby_vista() {
		return itHobby_vista;
	}


	public void setItHobby_vista(JMenuItem itHobby_vista) {
		this.itHobby_vista = itHobby_vista;
	}


	public JMenuItem getItLocalidades_vista() {
		return itLocalidades_vista;
	}


	public void setItLocalidades_vista(JMenuItem itLocalidades_vista) {
		this.itLocalidades_vista = itLocalidades_vista;
	}


	public JMenuItem getItTipoContacto_vista() {
		return itTipoContacto_vista;
	}


	public void setItTipoContacto_vista(JMenuItem itTipoContacto_vista) {
		this.itTipoContacto_vista = itTipoContacto_vista;
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}


	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String piso = p.getPiso();
			String departamento = p.getDepartamento();
			String localidad = p.getLocalidad();
			String email = p.getEmail();
			String fechaNacimiento = p.getFechaNacimiento();
			String tipoContacto = p.getTipoContacto();
			String hobby = p.getHobby();
			
			Object[] fila = {nombre, tel, calle, altura, piso, departamento, localidad, email, fechaNacimiento, tipoContacto,hobby};
			this.getModelPersonas().addRow(fila);
		}
		
	}
	
	public JButton getBtnReporte2() {
		return btnReporte2;
	}


	public void setBtnReporte2(JButton btnReporte2) {
		this.btnReporte2 = btnReporte2;
	}
}
