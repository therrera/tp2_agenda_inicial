package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.reportes.ReporteHobby;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaEditarPersona;
import presentacion.vista.VentanaHobbies;
import presentacion.vista.Vista;
import dto.HobbyDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;
import presentacion.vista.VentanaLocalidades;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener {

	private Agenda agenda;
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private List<LocalidadDTO> localidadesEnTabla;
	private List<TipoContactoDTO> tipoContactosEnTabla;
	private List<HobbyDTO> hobbiesParaDescolgable;
	private List<HobbyDTO> hobbiesEnTabla;
	
	private VentanaPersona ventanaPersona;
	private VentanaEditarPersona ventanaEditarPersona;
	private VentanaLocalidades ventanaLocalidad;
	private VentanaTipoContacto ventanaTipoContacto;
	private VentanaHobbies ventanaHobbies;
	private Date date2;

	public Controlador(Vista vista, Agenda agenda) {
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnEditar().addActionListener(h -> ventanaEditarPersona(h));
		this.vista.getBtnEditar().addActionListener(x -> obtenerPersona(x));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r));
		this.vista.getBtnReporte2().addActionListener(r -> mostrarReporte2(r));
		this.vista.getItLocalidades_vista().addActionListener(u -> abrirFormularioLocalidades(u));
		this.vista.getItTipoContacto_vista().addActionListener(u -> abrirFormularioTiposContacto(u));
		this.vista.getItHobby_vista().addActionListener(u -> abrirFormularioHobby(u));

		
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));

		this.ventanaEditarPersona = VentanaEditarPersona.getInstance();
		this.ventanaEditarPersona.getBtnEditarPersona().addActionListener(g -> editarPersona(g));

		this.ventanaLocalidad = VentanaLocalidades.getInstance();
		this.ventanaLocalidad.getBtnAgregar().addActionListener(t -> guardarLocalidad(t));
		this.ventanaLocalidad.getBtnModificar().addActionListener(b -> modificarLocalidad(b));
		this.ventanaLocalidad.getBtnEliminar().addActionListener(c -> eliminarLocalidad(c));

		this.ventanaTipoContacto = VentanaTipoContacto.getInstance();
		this.ventanaTipoContacto.getBtnAgregar().addActionListener(d -> guardarTipoContacto(d));
		this.ventanaTipoContacto.getBtnModificar().addActionListener(e -> modificarTipoContacto(e));
		this.ventanaTipoContacto.getBtnEliminar().addActionListener(f -> eliminarTipoContacto(f));

		this.ventanaHobbies = VentanaHobbies.getInstance();
		this.ventanaHobbies.getBtnAgregar().addActionListener(d -> guardarHobbie(d));
		this.ventanaHobbies.getBtnModificar().addActionListener(e -> modificarHobbie(e));
		this.ventanaHobbies.getBtnEliminar().addActionListener(f -> eliminarHobbie(f));

		
		
		this.agenda = agenda;
		refrescarTablaLocalidad();
		refrescarTablaTipoContacto();
		refrescarTablaHobby();
		llenarComboLocalidad();
		llenarComboTipoContacto();
		llenarComboHobbies();
	}

	
	private void ventanaAgregarPersona(ActionEvent a) {
	//	this.ventanaPersona.limpiarControles();
		this.ventanaPersona.mostrarVentana();
	}

	private void ventanaEditarPersona(ActionEvent h) {

		this.ventanaEditarPersona.mostrarVentana();
	}

	public void editarPersona(ActionEvent g) {

		PersonaDTO p = null;
		
		if(! this.ventanaEditarPersona.verificarCampos()) {
			JOptionPane.showMessageDialog(null, "Por favor revise los campos e intente nuevamente.");
		}
		else {

			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas) {

				p = this.personasEnTabla.get(fila);
				
				p.setNombre(this.ventanaEditarPersona.getTxtNombre().getText());
				p.setTelefono(this.ventanaEditarPersona.getTxtTelefono().getText());
				p.setCalle(this.ventanaEditarPersona.getTxtCalle().getText());
				p.setAltura(this.ventanaEditarPersona.getTxtAltura().getText());
				p.setPiso(this.ventanaEditarPersona.getTxtPiso().getText());
				p.setDepartamento(this.ventanaEditarPersona.getTxtDepartamento().getText());
				p.setEmail(this.ventanaEditarPersona.getTxtEmail().getText());
				p.setFechaNacimiento(this.ventanaEditarPersona.getFechaNacimiento());
				p.setLocalidad(this.ventanaEditarPersona.getjComboBoxLocalidad().getSelectedItem().toString());
				p.setTipoContacto(this.ventanaEditarPersona.getjComboBoxTipoContacto().getSelectedItem().toString());
				p.setHobby(this.ventanaEditarPersona.getCbHobby().getSelectedItem().toString());
			}
			
			this.agenda.editarPersona(p);		
			this.refrescarTabla();
			this.ventanaEditarPersona.cerrar();
		}

	}

	public void obtenerPersona(ActionEvent x) {
		
		PersonaDTO p = new PersonaDTO();
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			p = this.personasEnTabla.get(fila);
		}

		this.ventanaEditarPersona.getTxtNombre().setText(p.getNombre());
		this.ventanaEditarPersona.getTxtTelefono().setText(p.getTelefono());
		this.ventanaEditarPersona.getTxtCalle().setText(p.getCalle());
		this.ventanaEditarPersona.getTxtAltura().setText(p.getAltura());
		this.ventanaEditarPersona.getTxtPiso().setText(p.getPiso());
		this.ventanaEditarPersona.getTxtDepartamento().setText(p.getDepartamento());
		this.ventanaEditarPersona.getTxtEmail().setText(p.getEmail());
		String date = p.getFechaNacimiento();
		try {
			Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ventanaEditarPersona.getDateChooserFechaNacimiento().setDate(date2);
		this.ventanaEditarPersona.getjComboBoxLocalidad().setSelectedItem(p.getLocalidad());
		this.ventanaEditarPersona.getjComboBoxTipoContacto().setSelectedItem(p.getTipoContacto());

	}

	private void guardarPersona(ActionEvent p) {
		refrescarTablaLocalidad();
	
		 if( ! this.ventanaPersona.verificarCampos() ) {
			 JOptionPane.showMessageDialog(null, "Por favor revise los campos e intente nuevamente.");
		 }
		 else {
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String calle = ventanaPersona.getTxtCalle().getText();
			String altura = ventanaPersona.getTxtAltura().getText();
			String piso = ventanaPersona.getTxtPiso().getText();
			String departamento = ventanaPersona.getTxtDepartamento().getText();
			String email = ventanaPersona.getTxtEmail().getText();
			String fechaNacimiento = ventanaPersona.getFechaNacimiento();
			String localidad = ventanaPersona.getjComboBoxLocalidad().getSelectedItem().toString();
			String tipoContacto = ventanaPersona.getjComboBoxTipoContacto().getSelectedItem().toString();
			String hobby = ventanaPersona.getCbHobbie().getSelectedItem().toString();

			PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, calle, altura, piso, departamento, localidad,
					email, fechaNacimiento, tipoContacto,hobby);
			
			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		 }
	}

	private void mostrarReporte(ActionEvent r) {
		ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
		reporte.mostrar();
	}

	private void mostrarReporte2(ActionEvent r) {
		
		ReporteHobby reporte = new ReporteHobby(this.agenda.obtenerPersonasHobbies());
		reporte.mostrar();
	}
	
	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
		}

		this.refrescarTabla();
		
	}

	public void inicializar() {
		this.refrescarTabla();
		this.vista.show();
	} 

	private void guardarLocalidad(ActionEvent t) {
		String descripcion = this.ventanaLocalidad.getTxtDescripcion().getText();
		LocalidadDTO localidad = new LocalidadDTO();
		localidad.setDescripcion(descripcion);

		if (descripcion.equals("") == true) {
			this.ventanaLocalidad.mostrarMensaje("Por favor, ingrese una descripcion", "Localidad");
		} else {
			if (this.agenda.validarLocalidad(localidad) == false) {
				this.agenda.agregarLocalidad(localidad);
				this.ventanaLocalidad.getTxtDescripcion().setText("");
				this.ventanaLocalidad.mostrarMensaje("La localidad ha sigo agregada con exito", "Localidad");
				refrescarTablaLocalidad();
				llenarComboLocalidad();	
			} else {
				this.ventanaLocalidad.mostrarMensaje("Ya existe una localidad con ese nombre", "Localidad");
			}
		}
		}

	private void modificarLocalidad(ActionEvent t) {
		String id = this.ventanaLocalidad.getTxtId_ME().getText();
		if (id.equals("") == false) {
			String descripcion = this.ventanaLocalidad.getTxtDescripcion_ME().getText();

			LocalidadDTO localidad = new LocalidadDTO();
			localidad.setId(Integer.parseInt(id));
			localidad.setDescripcion(descripcion);
			this.agenda.modificarLocalidad(localidad);

			this.ventanaLocalidad.getTxtDescripcion_ME().setText("");
			this.ventanaLocalidad.getTxtId_ME().setText("");
			this.ventanaLocalidad.mostrarMensaje("La localidad ha sigo modificada con exito", "Localidad");
			refrescarTablaLocalidad();
			llenarComboLocalidad();
		}
	}

	private void abrirFormularioLocalidades(ActionEvent r) {
		this.ventanaLocalidad.mostrar();
	}

	private void abrirFormularioTiposContacto(ActionEvent r) {
		this.ventanaTipoContacto.mostrar();
	}
	private void abrirFormularioHobby(ActionEvent r) {
		this.ventanaHobbies.mostrar();
	}
	private void eliminarLocalidad(ActionEvent t) {
		String id = this.ventanaLocalidad.getTxtId_ME().getText();
		if (id.equals("") == false) {
			LocalidadDTO localidad = new LocalidadDTO();
			localidad.setId(Integer.parseInt(id));

			if(this.ventanaLocalidad.eliminarMensaje()==true)
			{
				this.agenda.borrarLocalidad(localidad);
				this.ventanaLocalidad.getTxtDescripcion_ME().setText("");
				this.ventanaLocalidad.getTxtId_ME().setText("");
				this.ventanaLocalidad.mostrarMensaje("La localidad ha sigo eliminada con exito", "Localidad");
				refrescarTablaLocalidad();
				llenarComboLocalidad();
				
			}
		}
	}

	private void guardarTipoContacto(ActionEvent t) {

		String descripcion = this.ventanaTipoContacto.getTxtDescripcion().getText();
		TipoContactoDTO tipoContacto = new TipoContactoDTO();
		tipoContacto.setDescripcion(descripcion);

		if (descripcion.equals("") == true) {
			this.ventanaTipoContacto.mostrarMensaje("Ingrese una descripcion", "Tipo contacto");
		} else {
			if (this.agenda.validarTipoContacto(tipoContacto) == false) {
				this.agenda.agregarTipoContacto(tipoContacto);
				this.ventanaTipoContacto.getTxtDescripcion().setText("");
				this.ventanaTipoContacto.mostrarMensaje("El tipo de contacto ha sigo agregado con exito",
						"Tipo contacto");
				refrescarTablaTipoContacto();
				llenarComboTipoContacto();
			} else {
				this.ventanaTipoContacto.mostrarMensaje("Ya existe un tipo de contacto con esa descripcion",
						"Tipo contacto");
			}
		}

	}

	private void modificarTipoContacto(ActionEvent t) {
		String id = this.ventanaTipoContacto.getTxtId_ME().getText();

		if (id.equals("") == false) {
			String descripcion = this.ventanaTipoContacto.getTxtDescripcion_ME().getText();

			TipoContactoDTO tipoContacto = new TipoContactoDTO();
			tipoContacto.setId(Integer.parseInt(id));
			tipoContacto.setDescripcion(descripcion);

			this.agenda.modificarTipoContacto(tipoContacto);

			this.ventanaTipoContacto.getTxtDescripcion_ME().setText("");
			this.ventanaTipoContacto.getTxtId_ME().setText("");
			this.ventanaTipoContacto.mostrarMensaje("El tipo de contacto ha sigo modificado con exito",
					"Tipo contacto");
			refrescarTablaTipoContacto();
			llenarComboTipoContacto();
		}
	}

	private void eliminarTipoContacto(ActionEvent t) {
		String id = this.ventanaTipoContacto.getTxtId_ME().getText();
		if (id.equals("") == false) {
			TipoContactoDTO tipoContacto = new TipoContactoDTO();
			tipoContacto.setId(Integer.parseInt(id));

			if(this.ventanaTipoContacto.eliminarMensaje()==true)
			{
			this.agenda.borrarTipoContacto(tipoContacto);

			this.ventanaTipoContacto.getTxtDescripcion_ME().setText("");
			this.ventanaTipoContacto.getTxtId_ME().setText("");
			this.ventanaTipoContacto.mostrarMensaje("El tipo de contacto ha sido eliminado con exito", "Tipo contacto");
			refrescarTablaTipoContacto();
			llenarComboTipoContacto();
			}
		}
	}

	
	private void guardarHobbie(ActionEvent t) {

		String descripcion = this.ventanaHobbies.getTxtDescripcion().getText();
		HobbyDTO hobby = new HobbyDTO();
		hobby.setDescripcion(descripcion);

		if (descripcion.equals("") == true) {
			this.ventanaHobbies.mostrarMensaje("Ingrese una descripcion", "Hobby");
		} else {
			if (this.agenda.validarHobbie(hobby) == false) {
				this.agenda.agregarHobbie(hobby);
				this.ventanaHobbies.getTxtDescripcion().setText("");
				this.ventanaHobbies.mostrarMensaje("El hobby ha sigo agregado con exito",
						"Hobbies");
				refrescarTablaHobby();
				llenarComboHobbies();
			} else {
				this.ventanaHobbies.mostrarMensaje("Ya existe hobby con esa descripcion",
						"Hobbies");
			}
		}

	}

	private void modificarHobbie(ActionEvent t) {
		String id = this.ventanaHobbies.getTxtId_ME().getText();

		if (id.equals("") == false) {
			String descripcion = this.ventanaHobbies.getTxtDescripcion_ME().getText();

			HobbyDTO hobby = new HobbyDTO();
			hobby.setId(Integer.parseInt(id));
			hobby.setDescripcion(descripcion);

			this.agenda.modificarHobbie(hobby);

			this.ventanaHobbies.getTxtDescripcion_ME().setText("");
			this.ventanaHobbies.getTxtId_ME().setText("");
			this.ventanaHobbies.mostrarMensaje("El Hobby ha sigo modificado con exito",
					"Hobby");
			refrescarTablaHobby();
			llenarComboHobbies();
		}
	}

	private void eliminarHobbie(ActionEvent t) {
		String id = this.ventanaHobbies.getTxtId_ME().getText();
		if (id.equals("") == false) {
			HobbyDTO hobby = new HobbyDTO();
			hobby.setId(Integer.parseInt(id));

			if(this.ventanaHobbies.eliminarMensaje()==true)
			{
				this.agenda.borrarHobbie(hobby);	
				this.ventanaHobbies.getTxtDescripcion_ME().setText("");
				this.ventanaHobbies.getTxtId_ME().setText("");
				this.ventanaHobbies.mostrarMensaje("El hobby ha sido eliminado con exito", "Hobby");
				refrescarTablaHobby();
				llenarComboHobbies();
			}
		}
	}

	
	private void refrescarTablaLocalidad() {
		this.localidadesEnTabla = agenda.obtenerLocalidad();
		this.ventanaLocalidad.llenarTabla(this.localidadesEnTabla);
	}

	private void llenarComboLocalidad() {
		this.localidadesEnTabla = agenda.obtenerLocalidad();
		this.ventanaPersona.llenarComboLocalidades(this.localidadesEnTabla);
		this.ventanaEditarPersona.llenarComboLocalidades(this.localidadesEnTabla);
	}

	private void llenarComboTipoContacto() {
		this.tipoContactosEnTabla = agenda.obtenerTipoContacto();
		this.ventanaPersona.llenarComboTipoContanto(this.tipoContactosEnTabla);
	    this.ventanaEditarPersona.llenarComboTipoContanto(this.tipoContactosEnTabla);

	}

	private void llenarComboHobbies() {
		this.hobbiesParaDescolgable = agenda.obtenerHobbies();
		this.ventanaPersona.llenarComboHobbies(this.hobbiesParaDescolgable);
	    this.ventanaEditarPersona.llenarComboHobbies(this.hobbiesParaDescolgable);

	}
	
	private void refrescarTablaTipoContacto() {
		this.tipoContactosEnTabla = agenda.obtenerTipoContacto();
		this.ventanaTipoContacto.llenarTabla(this.tipoContactosEnTabla);
	}

	private void refrescarTablaHobby() {
		this.hobbiesEnTabla = agenda.obtenerHobbies();
		this.ventanaHobbies.llenarTabla(this.hobbiesEnTabla);
	}
	
	
	
	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
