package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.HobbyDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.HobbyDAO;

public class HobbyDAOSQL implements HobbyDAO{

	
	private static final String insert = "INSERT INTO hobby(descripcion) VALUES(?)";
	private static final String delete = "DELETE FROM hobby WHERE id = ?";
	private static final String modify = "UPDATE hobby SET descripcion=? WHERE id = ?";
	private static final String readall = "SELECT * FROM hobby";
	private static final String valid = "SELECT * FROM hobby where descripcion=?";
	
	
	@Override
	public boolean insert(HobbyDTO hobby) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, hobby.getDescripcion());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public boolean delete(HobbyDTO hobby_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(hobby_a_eliminar.getId()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public boolean update(HobbyDTO hobby_a_modificar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(modify);
			statement.setInt(2, hobby_a_modificar.getId());
			statement.setString(1, hobby_a_modificar.getDescripcion());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public List<HobbyDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<HobbyDTO> hobbies = new ArrayList<HobbyDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				hobbies.add(getHobbyDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return hobbies;
	}

	@Override
	public boolean validate(HobbyDTO hobby) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(valid);
			statement.setString(1, hobby.getDescripcion());
			ResultSet rs = statement.executeQuery();
			if(rs.next())
			{
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	private HobbyDTO getHobbyDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String descripcion = resultSet.getString("descripcion");
		return new HobbyDTO(id, descripcion);
	}

	@Override
	public ArrayList<HobbyDTO> readAllHobbyPersons() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<HobbyDTO> lista = new ArrayList<HobbyDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				HobbyDTO HobbieLeido = getHobbyDTO(resultSet);
				PersonaDAOSQL personaDao = new PersonaDAOSQL();
				ArrayList<PersonaDTO> lPersonas = (ArrayList<PersonaDTO>) personaDao.getPersonas(HobbieLeido);
				HobbieLeido.setListaPersonas(lPersonas);
				lista.add(HobbieLeido);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return lista;
	}

}
