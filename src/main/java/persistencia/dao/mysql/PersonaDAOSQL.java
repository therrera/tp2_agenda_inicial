package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.HobbyDTO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, calle, altura, piso, departamento, localidad, email, fechaNacimiento, tipoContacto,hobby) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
	private static final String update = "UPDATE personas SET nombre = ?, telefono = ?, calle = ?, altura = ?, piso = ?, departamento = ?, localidad = ? , email = ?, fechaNacimiento = ?, tipoContacto = ?, hobby=? WHERE idPersona = ?";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
	private static final String read = "SELECT * FROM personas WHERE idPersona = ?";
	private static final String readPersonHobby = "SELECT * FROM personas WHERE Hobby = ? order by nombre asc";
	
	
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getCalle());
			statement.setString(5, persona.getAltura());
			statement.setString(6, persona.getPiso());
			statement.setString(7, persona.getDepartamento());
			statement.setString(8, persona.getLocalidad());
			statement.setString(9, persona.getEmail());
			statement.setString(10, persona.getFechaNacimiento());
			statement.setString(11, persona.getTipoContacto());
			statement.setString(12, persona.getHobby());
			
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public boolean update(PersonaDTO persona_a_editar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona_a_editar.getNombre());
			statement.setString(2, persona_a_editar.getTelefono());
			statement.setString(3, persona_a_editar.getCalle());
			statement.setString(4, persona_a_editar.getAltura());
			statement.setString(5, persona_a_editar.getPiso());
			statement.setString(6, persona_a_editar.getDepartamento());
			statement.setString(7, persona_a_editar.getLocalidad());
			statement.setString(8, persona_a_editar.getEmail());
			statement.setString(9, persona_a_editar.getFechaNacimiento());
			statement.setString(10, persona_a_editar.getTipoContacto());
			statement.setString(11, persona_a_editar.getHobby());
			statement.setInt(12, persona_a_editar.getIdPersona());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isEditExitoso = true;
			}
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isEditExitoso;
	}

	public PersonaDTO read(PersonaDTO persona_a_leer) {
		PreparedStatement statement;
		ResultSet result;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		PersonaDTO persona_leida = new PersonaDTO();
		
		try {
			statement = conexion.prepareStatement(read);
			result =  statement.executeQuery(read);
			
			persona_leida = getPersonaDTO(result);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return persona_leida;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String calle = resultSet.getString("Calle");
		String altura = resultSet.getString("Altura");
		String piso = resultSet.getString("Piso");
		String departamento = resultSet.getString("Departamento");
		String localidad = resultSet.getString("Localidad");
		String email = resultSet.getString("Email");
		String fechaNacimiento = resultSet.getString("FechaNacimiento");
		String tipoContacto = resultSet.getString("TipoContacto");
		String hobbie = resultSet.getString("Hobby");
		return new PersonaDTO(id, nombre, tel,calle, altura, piso, departamento, localidad, email, fechaNacimiento, tipoContacto,hobbie);
	}

	@Override
	public List<PersonaDTO> getPersonas(HobbyDTO hobby) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readPersonHobby);
			statement.setString(1, hobby.getDescripcion());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
}
