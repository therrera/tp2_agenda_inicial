package persistencia.dao.interfaz;

import java.util.List;

import dto.HobbyDTO;
import dto.PersonaDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);
	
	public boolean update(PersonaDTO persona_a_editar);
	
	public PersonaDTO read(PersonaDTO persona_a_leer);
	
	public List<PersonaDTO> readAll();
	
	public List<PersonaDTO> getPersonas(HobbyDTO hobby);
}
