package persistencia.dao.interfaz;

import java.util.List;

import dto.HobbyDTO;


public interface HobbyDAO {

	public boolean insert(HobbyDTO hobby);

	public boolean delete(HobbyDTO hobby_a_eliminar);
	
	public boolean update(HobbyDTO hobby_a_modificar);
	
	public List<HobbyDTO> readAll();

	boolean validate(HobbyDTO hobby);
	
	public List<HobbyDTO> readAllHobbyPersons();
}
