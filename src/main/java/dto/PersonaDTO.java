package dto;

public class PersonaDTO {
	private int idPersona;
	private String nombre;
	private String telefono;
	private String calle;
	private String altura;
	private String piso;
	private String departamento;
	private String email;
	private String fechaNacimiento;
	private String localidad;
	private String tipoContacto;
	private String hobby;


	public PersonaDTO() {
		super();
	}
	
	public PersonaDTO(int idPersona, String nombre, String telefono, String calle, String altura, String piso,
			String departamento, String localidad, String email, String fechaNacimiento, String tipoContaco,String hobbie) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.departamento = departamento;
		this.localidad = localidad;
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
		this.tipoContacto = tipoContaco;
		this.hobby= hobbie;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	
	public int getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public String getLocalidad() {
		return localidad;
	}
	
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	public String getTipoContacto() {
		return tipoContacto;
	}
	
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	@Override
	public String toString() {
		return   nombre +" "+ telefono + " "
				+ calle + " " + altura + " " + piso + " " + departamento + " "
				+ email + " " + fechaNacimiento + " " + localidad + " "
				+ tipoContacto;
	}

	
}
