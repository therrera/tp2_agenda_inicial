package dto;

import java.util.ArrayList;

public class HobbyDTO {

	private int id;
	private String descripcion;
	private ArrayList<PersonaDTO> listaPersonas;

	public HobbyDTO()
	{
		
	}
	
	public HobbyDTO(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public ArrayList<PersonaDTO> getListaPersonas() {
		return listaPersonas;
	}

	public void setListaPersonas(ArrayList<PersonaDTO> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}
	
	

}
